<?php
/*In this routes class we are communicating with our standard and resource controllers knowing where
 * to go to display our views with the appropriate data.
 * It will search for the particular method below, for example 'processLoan' in the librarian controller
 * We are using these routes to connect to all of our 9 controllers.
 * 
 * 
 */


/*
 *
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
this is a test commit to see if it uploads to git
|
*/

//App::bind('AuthorService', 'larkin\service\impl\AuthorServiceImpl');

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('nowtime', 'TimeController@now');

//Route::get('books', 'BookController@listAll');

Route::get('firstbook', 'BookController@getFirst');

Route::get('authors', 'AuthorController@listAuthors') ->before('auth');

//communicating with the listDetails function in the Home Controller when home view is returned
Route::post('home', 'HomeController@listDetails');
Route::get('home', 'HomeController@listDetails');
//communicating with the index function in the Admin Controller when home fines view is returned
Route::get('fines', 'AdminController@index');

//Route::post('book', 'BookController@add');
/*
Route::get('books', function()
{
	$books = Book::all();

	//$book = Book::where('isbn', '345353535')->get();
	foreach($book as $b) {
		echo $b->title . ' (' . $author->name . ') <br/>';
	}
	
	return View::make('booklist')->with('books', $books);
});
*/
//Route::get('authors', array('before' => 'auth'), 'AuthorController@listAuthors');
Route::get('authors', 'AuthorController@listAuthors')->before('auth');
//Route::get('categories','BookController@showCategories');
Route::group(array('prefix' => LaravelLocalization::setLocale()), function()
{
	Route::resource('book', 'BookController');
});

//for displaying our category and all the books contained
//Route::resource('category','CategoryController');

// Authentication/authorisation routes
Route::resource('search', 'SearchController');
Route::post('searchindex', 'SearchController@index');
//Route::post('search', 'SearchController@show');
Route::get('login', 'SecurityController@showLogin');

//communicating with the login,logout,signup & update functions using post and get requests to the Security Controller when home login,logout,signup views are requested

Route::post('login', 'SecurityController@doLogin');
Route::get('logout', 'SecurityController@doLogout');
Route::get('signup', 'SecurityController@doSignup') ->before('auth');
Route::post('update', 'SecurityController@update');
//Route::get('librarian', 'SecurityController@makeLibrarian');
Route::get('member', 'SecurityController@makeMember');
Route::get('admin', 'SecurityController@makeAdmin');

//communicating with the displayMembers and displayMemberDetails functions in the Librarian Controller when home librarian and memberdetails are requested
Route::get('librarian', 'LibrarianController@displayMembers');
Route::post('memberdetails', 'LibrarianController@displayMemberDetails');
Route::resource('category', 'CategoryController');
//Route::get('category', 'CategoryController@index');
Route::get('processLoan', 'LibrarianController@processLoans');
//Route::resource('book', 'BookController');
Route::post('loanUpdate', 'LibrarianController@loanUpdate');

Route::resource('review', 'ReviewController');
Route::resource('adminC', 'AdminController');

Route::post('reviewupdate', 'ReviewController@update');


