@extends('layout')
<!--  This view displays all the different types of categories in a hyperlink format.-->
@section('header')
Category
@stop

@section('leftmenu')
@parent
@stop

@section('content')

@for ($i = 0; $i < count($categories); $i++)
    	<p><a href="{{{URL::to('category')}}}/{{{$categories[$i]->id}}}">{{$categories[$i]->category_name}}</a></p>
	@endfor
@stop