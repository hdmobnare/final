@extends('layout')
<!--  This view displays all the books in a particular category-->
@section('header')
	<p>Category: {{{$category}}}</p>
@stop

@section('leftmenu')
	@parent
@stop

@section('content')


	
	<p>All books in this category:</p>
	
	@if(count($books) < 1)
		<p>No book found at all!!!</p>
	@elseif(count($books) == 1)
		<p>Only a single book found!</p>
	@else
		<p>{{count($books)}} {{trans('books.list_numfound')}}.</p>
	@endif
	
	@for ($i = 0; $i < count($books); $i++)
    	<a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}">
         {{{$books[$i]->title}}}</a> [<a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}/edit">{{trans('labels.edit')}}</a>]<br/> 
	@endfor
@stop