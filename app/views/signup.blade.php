@extends('layout')
<!--  Here is the blade which provides the view to enable a new user to register and signup to the library using a form to capture all their details-->
@section('header')
	Please Sign up
@stop

@section('content')
	{{Form::open(array('url' => 'update'))}}
		<!-- List any login errors -->
		@if($errors->has())
			<ul>
				@foreach ($errors->all() as $error)
			 	<li>{{ $error }}</li>
				@endforeach
			</ul>
		@endif

		<p>
			{{Form::label('username', 'Username')}}
			{{Form::text('username', '')}}
		</p>

		<p>
			{{Form::label('password', 'Password')}}
			{{Form::password('password')}}
		</p>

		<p>
			{{Form::label('email', 'Email')}}
			{{Form::text('email', '')}}
		</p>
		
				<p>
			{{Form::label('name', 'Name')}}
			{{Form::text('name', '')}}
		</p>
				<p>
			{{Form::label('address', 'Address')}}
			{{Form::text('address', '')}}
		</p>
		<p>
			{{Form::label('phone', 'Phone')}}
			{{Form::text('phone', '')}}
		</p>
		<p>{{Form::submit('Register') }}</p>
	{{Form::close()}}
@stop