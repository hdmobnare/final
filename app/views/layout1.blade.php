<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/main.css">
	</head>
    <body>
    	<div id="topBar">
	    	@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
	        
	            <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
	                {{{ $properties['native'] }}}
	            </a>
	        |
    		@endforeach
    		<a href="{{ URL::to('home') }}">Home</a>
    		<a href="{{ URL::to('logout') }}">Logout</a>
    		<a href="{{ URL::to('search') }}">Search</a>
    	</div>
        <h1>@yield('header')</h1>
		
	
		<div id="leftMenu">
        @section('leftmenu')
        	<p><a href="{{URL::to('book')}}">Browse All</a></p>
      	<p><a href="{{URL::to('category')}}">Browse by Category</a></p>
        	
        @show
        </div>
        
        @yield('content')
    </body>
</html>
