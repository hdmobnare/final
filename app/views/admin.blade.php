@extends('layout')
<!--  This is the admin view blade which has 2 forms, enabling the administrator adjust the loan period, the fines and the book allowance
It also allows the administrator to adjust the number of recently added books, number of most borrowed books and number of highest rated books -->
@section('header')
	Administrators Page
@stop
@section('content')
<p><a href="{{URL::to('fines')}}">View fines report</a></p>
	
	{{Form::open(array('url' => 'adminC'))}}
         Change the loan period:
         <p>
			{{Form::label('loanperiod', 'Loan Period:')}}
			{{Form::text('loanperiod', '2')}}weeks
		</p>
		
		Change the fine per day:
         <p>
			{{Form::label('fine', 'Fine: EUR')}}
			{{Form::text('fine', '0.05')}}
		</p>
		
		Change the book allowance:
         <p>
			{{Form::label('ba', 'Book allowance')}}
			{{Form::text('ba', '4')}}
		</p>
         
	<p>{{Form::submit('Submit')}}</p>
{{Form::close()}}	

	Change home page display:</br></br>
	{{Form::open(array('url' => 'home'))}}
         Change the number of recently added books:
         <p>
			{{Form::label('rab', 'Number of recently added books:')}}
			{{Form::text('rab', '10')}}
		</p>
		
		Change the number of most borrowed books:
         <p>
			{{Form::label('mbr', 'Number of most borrowed books')}}
			{{Form::text('mbr', '5')}}
		</p>
		
		Change the number of highest rated books:
         <p>
			{{Form::label('hrb', 'Number of highest rated books')}}
			{{Form::text('hrb', '4')}}
		</p>
         
	<p>{{Form::submit('Submit')}}</p>
	
{{Form::close()}}
@stop	
	
	