@extends('layout')
<!--  This view displays all the books with an edit hyperlink next to them -->
@section('header')
	{{trans('books.list_title')}}
@stop

@section('leftmenu')
	@parent
	<p><a href="{{URL::to('nowtime')}}">Current Time</a></p>
@stop

@section('content')

	@if(count($books) < 1)
		<p>No book found at all!!!</p>
	@elseif(count($books) == 1)
		<p>Only a single book found!</p>
	@else
		<p>{{count($books)}} {{trans('books.list_numfound')}}.</p>
	@endif
	
	@for ($i = 0; $i < count($books); $i++)
    	<a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}">
         {{{$books[$i]->title}}}</a> [<a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}/edit">{{trans('labels.edit')}}</a>]<br/> 
	@endfor
	
	{{-- 
	@foreach($books as $book)
		{{{$book->title}}} <br/>
	@endforeach
	--}}
@stop
