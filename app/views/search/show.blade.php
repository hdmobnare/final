@extends('layout1')
<!-- This view creates the search page where we have text boxes to search by title and by author  -->
@section('header')
	SEARCH PAGE
@stop

@section('content')


	{{Form::open(array('url' => 'searchindex'))}}
		<!-- List any login errors -->
		@if($errors->has())
			<ul>@foreach ($errors->all() as $error)
			 	<li>{{ $error }}</li>
				@endforeach
			</ul>
		@endif

		<p>
			{{Form::label('title', 'Search by title:')}}
			{{Form::text('title', '')}}
		</p>

		<p>
			{{Form::label('author', 'Search by author:')}}
			{{Form::text('author','')}}
		</p>
		<p>{{Form::submit('Search') }}</p>
	{{Form::close()}}
@stop