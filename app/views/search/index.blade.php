@extends('layout1')
<!-- This view creates the search page where we have text boxes to search by title and by author and informs the user of the number of those books available -->
@section('header')
	SEARCH PAGE
@stop

@section('content')
@foreach ($result as $book)
		<li>{{ $book->title }}</li>
		 <li>No. available: {{ $book->in_stock }}</li>
@endforeach
	{{Form::open(array('url' => 'searchindex'))}}
		<!-- List any login errors -->
		@if($errors->has())
			<ul>
				@foreach ($errors->all() as $error)
			 	<li>{{ $error }}</li>
				@endforeach
			</ul>
		@endif

		<p>
			{{Form::label('title', 'Search by title:')}}
			{{Form::text('title', '')}}
		</p>

		<p>
			{{Form::label('author', 'Search by author:')}}
			{{Form::text('author','')}}
		</p>
		<p>{{Form::submit('Search') }}</p>
	{{Form::close()}}
@stop