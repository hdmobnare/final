@extends('layout')
<!-- This blade layout is creating the view to enable a user to make a review of a book selecting a rating and writing a comment using a form -->
@section('header')
	Review {{{$book->title}}
@stop

@section('leftmenu')

@section('content')

@if($errors->has())
	<ul>
		@foreach ($errors->all() as $error)
	 	<li>{{ $error }}</li>
		@endforeach
	</ul>
@endif

{{Form::model($book, array('route' => array('review.update', $book->id), 'method' => 'put'))}}
	
	<p>Rating: {{Form::selectRange('Review', 1,5)}}</p>
	
	<p>ID: {{Form::label('id', $book->id)}}</p>
	
	<p>Title: {{Form::text('title')}}</p>
	<p>Comments: {{Form::textarea('comments')}}</p>
	

	<p>{{Form::submit('Submit Review')}}</p>
	
{{Form::close()}}
@stop