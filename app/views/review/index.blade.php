@extends('layout')
<!-- This blade layout is to display the review link next to each book to enable users to review a book -->
@section('header')
	Display Reviews index
@stop

@section('leftmenu')
	@parent
	<p><a href="{{URL::to('nowtime')}}">Current Time</a></p>
@stop

@section('content')

	
	@for ($i = 0; $i < count($books); $i++)
    	<a href="{{{URL::to('book')}}}/{{{$books[$i]->id}}}">
         {{{$books[$i]->title}}}</a> 
         [<a href="{{{URL::to('review')}}}/{{{$books[$i]->id}}}/edit">Review</a>]<br> 
         
	@endfor
	
	{{-- 
	@foreach($books as $book)
		{{{$book->title}}} <br/>
	@endforeach
	--}}
@stop