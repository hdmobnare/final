@extends('layout')
<!--  This view presents the dynamic details which we have on our home page with the highest rated titles, the most recently added titles and the most borrowed titles -->
<a href="{{ URL::to('login') }}">Login</a>
@section('header')
LARAVELLER LIBRARY HOME PAGE</br>

@stop

@section('content')
The {{{$mbr}}} most borrowed titles are: </br>
<p><ul>
	@for($i = 0; $i<$mbr; $i++)
		<li>{{$mostborrowed[$i]->title}} </li>
		@endfor
	</ul>
</p>
The {{{$hrb}}} highest rated titles are: </br>
<p><ul>
	@for($i = 0; $i<$hrb; $i++)
		<li>{{$highestrated[$i]->title}} </li>
		@endfor
</ul></p>

The {{{$rab}}} most recently added titles are: </br>
<p><ul>@for($i = 0; $i<$rab; $i++)
		<li>{{$recently_added[$i]->title}} </li>
		@endfor
</ul></p>
<p>
Number of members: {{$count}}
</p>
@stop