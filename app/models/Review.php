<?php
//A Standard Model class which sets up a relationship between books and reviews
class Review extends Eloquent {

	public function books()
	{
		return $this->belongTo('Book');
	}

}
?>