<?php
//A Standard Model class which sets up a relationship between books, authors and category
class Book extends Eloquent {
	
	private $id;
//	public $in_stock;
	
	
	
	public function author() {
		return $this->belongsTo('Author');
	}
	
	public static function validate($inputs) {
		$rules = array(
			'title' 	=> 'Required',
			'isbn'		=> 'Required|Min:9|Max:15'
		);
	
		return Validator::make($inputs, $rules);
	}
	
	public function category() {
		return $this->belongsTo('Category');
	}
	
/* 	public function getInStock() {
		return $this->in_stock;
	}
	
	public function setInStock($in_stock) {
		$this->in_stock = $in_stock;
	} */
}
?>
