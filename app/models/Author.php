<?php
//A Standard Model class which sets up a relationship between Authors and books
class Author extends Eloquent {

	public function books()
	{
		return $this->hasMany('Book');
	}

}
?>