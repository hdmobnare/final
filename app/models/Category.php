<?php
//A Standard Model class which sets up a relationship between category and books
class Category extends Eloquent {

	public function books()
	{
		return $this->hasMany('Book');
	}

}
?>