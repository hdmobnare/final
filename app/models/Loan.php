<?php
// use Illuminate\Auth\UserInterface;
// use Illuminate\Auth\Reminders\RemindableInterface;
//A Standard Model class which sets up a loan object with a userId,bookId and uniqueId.

class Loan extends Eloquent{

	private $userid;
	private $bookid;
	private $uniqueid;
	
	public function getUserId() {
		return $this->userid;
	}
	
	public function setUserId($userid) {
		$this->userid = $userid;
	}

	public function getBookId() {
		return $this->userid;
	}
	
	public function setUniqueId($uniqueid) {
		$this->uniqueid = $uniqueid;
	}
	
	public function getUniqueId() {
		return $this->uniqueid;
	}
	
	public function setBookId($bookid) {
		$this->bookid = $bookid;
	}
}