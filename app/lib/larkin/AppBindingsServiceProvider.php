<?php
namespace larkin;

use Illuminate\Support\ServiceProvider;

class AppBindingsServiceProvider extends ServiceProvider {

	/**
	 * Register the binding
	 *
	 * @return void
	 */
	public function register() {

		$this->app->bind('larkin\service\AuthorService', 'larkin\service\impl\AuthorServiceImpl');
		
		$this->app->bind('larkin\repository\AuthorRepository', 'larkin\repository\impl\DbAuthorRepository');
	}

}