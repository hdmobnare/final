<?php
namespace larkin\mapper;

use larkin\Loan;

class LoanRowMapper implements RowMapper {
	
	function mapRow($row) {
		
		$loan = new Loan();
		$loan->setId($row->id);
	
		return $loan;
	}
	
	function mapRowToArray($rows) {
		
		$loans = array();
		
		foreach($rows as $row) {
			$loan = $this->mapRow($row);
			$loans[] = $loan;
		}
		
		return $loans;
	}
}