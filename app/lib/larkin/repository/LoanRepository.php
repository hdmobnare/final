<?php
namespace larkin\repository;

interface LoanRepository {
	public function getById($id);
	public function getAll();
}