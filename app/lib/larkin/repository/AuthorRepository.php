<?php
namespace larkin\repository;

interface AuthorRepository {
	public function getById($id);
	public function getAll();
}