<?php
namespace larkin\service\impl;

use larkin\service\LoanService;
use larkin\repository\LoanRepository;

class LoanServiceImpl implements LoanService {

	private $loanRepository;
	
	function __construct(LoanRepository $loanRepository) {
		$this->loanRepository = $loanRepository;
	}
	
	function getById($id) {
		return $this->loanRepository->getById($id);
	}
	
	function getAll() {
		return $this->loanRepository->getAll();
	}
}