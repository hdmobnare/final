<?php
namespace larkin\service;

interface AuthorService {
	public function getById($id);
	public function getAll();
}