<?php

// make the PDO connection availabce app-wide
$datasource = DB::connection()->getPdo();
App::instance('datasource', $datasource);

App::bind('AuthorRepository', 'DbAuthorRepository');

App::bind('AuthorService', 'AuthorServiceImpl');