<?php
/*
 * This is a resource controller.  It is enabling the user to search for books by title and author.
 * This class is sending data to the index and show views.
 */
class SearchController extends \BaseController {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	/*
	 * This function enables us to search for a book by title and by author.
	 * We create a new array of books as an array is expected by view and not a book object.
	 * If a title matches what the user entered then that book is displayed
	 * and if there is not a book title or author matching what is entered then we tell the user that
	 * there is no such book or author.
	 * We are querying our book table in our database for the title and author columns.
	 */
	public function index() {
		$result = array ();
		if (Input::has ( 'title' )) {
			// $id = DB::select('SELECT id FROM "books" where title = ?', array(Input::get('title')));
			$id = DB::table ( 'books' )->where ( 'title', Input::get ( 'title' ) )->pluck ( 'id' );
			if ($id == null) {
				$book = new Book ();
				$book->title = "This title is unavailable - try again";
				$book->in_stock = 0;
				$result [] = $book;
			} else
				$result [] = Book::find ( $id );
		} else if (Input::has ( 'author' )) {
			$ids = DB::select ( 'SELECT id FROM books where author_id = (select 
				id from authors where name = ?)', array (
					Input::get ( 'author' ) 
			) );
			if ($ids == null) {
				$book = new Book ();
				$book->title = "No such authors books in stock - try again";
				$book->in_stock = 0;
				$result [] = $book;
			} else {
				foreach ( $ids as $id )
					$result [] = Book::find ( $id->id );
			}
		}
			return View::make ( 'search.index' )->with ( 'result', $result );
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		//
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	//This function makes our show view
	public function show() {
		return View::make ( 'search.show' );
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function edit($id) {
		//
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update() {
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function destroy($id) {
		//
	}
}