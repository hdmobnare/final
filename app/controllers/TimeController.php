<?php
class TimeController extends BaseController {
	public function now()
	{
		return View::make('nowtime')->with('time', date('h:i:s a', time()));
	}
}
