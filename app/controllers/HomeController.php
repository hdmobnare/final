<?php
/*
 * This controller is controlling our Home page view. It displays our dynamic details such as 
 * the 5 most borrowed books,  the 4 highest rated books, the 10 most recently added books as well our number of members.
 */
class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}
	/*
	 * This function listDetails is running 3 queries on our database loans table, reviews table and books table,storing them in arrays
	 * to provide the dynamic details for our website.
	 */
	public function listDetails(){
		$count = User::get()->count();
		$temp1 = DB::select('SELECT bookid, count(id) as count FROM "loans" 
				group by bookid order by count desc');
		$mostborrowed = array();
		foreach ($temp1 as $t){
			$mostborrowed[] = Book::find($t->bookid);
		}
		
		$temp2 = DB::select('SELECT book_id, sum(rating) as sum FROM reviews group by book_id 
				order by sum desc');
		$highestrated = array();
		foreach ($temp2 as $t){
			$highestrated[] = Book::find($t->book_id);
		}
		
		$temp3 = DB::select('SELECT id FROM "books" order by created_at desc');
		$recently_added = array();
		foreach ($temp3 as $t){
			$recently_added[] = Book::find($t->id);
		}
		
		//Here we are getting the values entered in by the administrator from a text file
		//to display the number of books we want displayed. So the Administrator can change thes values
		//from 10 recently added books, 4 highest rated books and 5 most borrowed books to other values if required.
 		$str = file_get_contents("settings.txt");
 		$arr = explode(",", $str);
		//Use settings.txt default values if administrator has not explicitly set them!
		$rab = count($recently_added)>Input::get('rab',$arr[0])?Input::get('rab',$arr[0]):count($recently_added);
		$mbr = count($mostborrowed)>Input::get('mbr', $arr[1])?Input::get('mbr', $arr[1]):count($mostborrowed);
		$hrb = count($highestrated)>Input::get('hrb',$arr[2])?Input::get('hrb',$arr[2]):count($highestrated);
		
		file_put_contents("settings.txt", $rab.",".$mbr.",".$hrb);
		
		return View::make('home')->with('count', $count)->with('mostborrowed', $mostborrowed)
		->with('highestrated', $highestrated)->with('recently_added', $recently_added)
		->with('rab', $rab)->with('mbr', $mbr)->with('hrb', $hrb);
	}

}