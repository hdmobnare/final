<?php
/*
 * This is a RESTful resource controller.  It is enabling us to browse our books by category and grouping our books into
 * appropriate cateogries based on our database tables.
 * It has the standard index, edit, show, store, create, destroy and update functions provided.
 * It is linking with index and show view classes to provide them with the category data to be displayed.
 */
class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	//This function is getting all of our categories so they can be displayed in our main cateogry view
	public function index()
	{
		$categories = Category::all();
		
		return View::make('category.index')->with('categories', $categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//This function is showing all the books that fall under a particular category in our show view
	public function show($id)
	{
		$books = Book::where('category_id', $id)->get();
		$category = Category::find($id);
		$category_name = $category->category_name;
 		return View::make('category.show')->with('category', $category_name)->with('books', $books);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}