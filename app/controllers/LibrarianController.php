<?php
/*
 * This is a standard controller runs as soons as the librarian logs in to check for fines from the database from members
 * and will display these details for the librarian to see those with fines to pay.
 */
class LibrarianController extends BaseController {
	
// 	displayMembers checks the status of all users and their corresponding loans transactions on the database
// 	 and verifies them prior to opening the librarians page. The assumption here is that the librarians are
// 	 the only ones handling these transactions.

	// This function will check for fines from members if they have books out longer than 14 days using
	//the julianday method.
	//$result= all the books out on loan from the database and a for loop is used to check for fines
	public function displayMembers() {
		
		$message = Session::get('message', '');
		$result = DB::select ( 'select *, (julianday(Date("now")) - julianday(loans.created_at)) as diff , 
				(julianday(Date("now")) - julianday(users.created_at)) as diffuser from users, loans 
				where loans.userid = users.id' );

		$rate;
		
		foreach ( $result as $row ) {
			if (($row->diffuser > 365) & $row->Fine_amount == 0)
				DB::update ( 'update users set Book_allowance = 6 where id = ?', array (
						$row->userid
				) );
			$rate = $row->Fine_rate;
			$days = $row->diff;
			$amountDue = $days > 14 ? ($days - 14) * $rate : 0;
			DB::update ( 'update loans set Fine_amount = ? where id = ?', array (
					$amountDue,
					$row->id 
			) );
		}
		
		$display = DB::table ( 'users' )->where ( 'role', null )->get ();
		// $temp = $display->where('role', null)->get();
		// $temp=$display
		/*
		 * $temp; foreach ($display as $member){ if($display->role==null){ } }
		 */
		return View::make ( 'librarian' )->with ( 'memberslist', $display )
		->with('message', $message);
	}
	
	//Where we have new members who have registered and have not yet been approved by the librarian
	// their details will be displayed for the librarian to APPROVE them based on the details they have submitted
	// Where the users role is still 'null' which means they have not yet been approved, once approved this will be updated to'member'
	//in our database which will enable the user to log in themselves after their approval.
	public function displayMemberDetails() {
		$display = DB::table ( 'users' )->whereNull ( 'role' )->update ( array (
				'role' => 'member' 
		) );
		$display1 = DB::table ( 'users' )->where ( 'role', null )->get ();
		return View::make ( 'librarian' )->with ( 'memberslist', $display1 );
	}
	public function processLoans() {
		return View::make ( 'loans' );
	}
	// This function is managing our loans which are created when loaning out a book
	// We create a new loan object with the values which the librarian has entered, book title, userId and UniqueId.
	//The $arr takes out all the uniqueIds from the books table and explodes these using a dilimiter with the "".
	//Then a new array is created with the $resutlarr where we add each uniqueId to this array using a for loop.
	//Finally we implode these uniqueID values ie glue everything back together in the database and the UniqueId the librarian entered will be extracted from the books table
	// this enables us to track the precise book the member has taken out an also decreases our books in stock value in the database.
	public function loanUpdate() {
		$uid = DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->pluck ( 'uniqueid' );
		$arr = explode ( ",", $uid );
		
		if (Input::has ( 'loan' )) {
			if(User::find(Input::get ( 'userid' ))->Book_allowance==0){

				return Redirect::to ( 'librarian' )->with('message', 'Book allowance has been 
						exceeded - transaction not processed!');
			}
			
			$loan = new Loan ();
			$loan->userid = Input::get ( 'userid' );
			$loan->bookid = Input::get ( 'titles' );
			$loan->uniqueid = Input::get ( 'uniqueid' );
			// $loan->bookid = Book::get()->lists('title', 'id', '')
			$loan->updated_at = null;
			$loan->save ();
			
			$arr = explode ( ",", $uid );
			$resultarr;
			foreach ( $arr as $unid ) {
				if (Input::get ( 'uniqueid' ) != $unid)
					$resultarr [] = $unid;
			}
			$result = implode ( ",", $resultarr );
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->update ( array (
					'uniqueid' => $result 
			) );
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->decrement ( 'in_stock' );
			DB::table ( 'users' )->where ( 'id', Input::get ( 'userid' ) )->decrement ( 'Book_allowance' );
		} 

		else {
			array_push ( $arr, Input::get ( 'uniqueid' ) );
			$result = implode ( ",", $arr );
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->update ( array (
					'uniqueid' => $result 
			) );
			
			DB::table ( 'books' )->where ( 'id', Input::get ( 'titles' ) )->increment ( 'in_stock' );
			DB::table ( 'users' )->where ( 'id', Input::get ( 'userid' ) )->increment ( 'Book_allowance' );
		}
		
		return Redirect::to ( 'librarian' );
	}
}