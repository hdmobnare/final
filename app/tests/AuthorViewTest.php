<?php

class AuthorViewTest extends TestCase {

	/**
	 * Test the author list view
	 *
	 * @return void
	 */
	public function testBookShowView()
	{
		$response = $this->call('GET', '/book/1');
		
		$this->assertContains('2323444442342', $response->getContent());
	}
}	