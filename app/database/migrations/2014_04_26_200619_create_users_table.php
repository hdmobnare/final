<?php
//Here we created our users table using the migration functionality
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateUsersTable extends Migration {
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'users', function ($table) {
			$table->string ( 'role' )->nullable ();
			$table->string ( 'name' )->nullable ();
			$table->string ( 'address' )->nullable ();
			$table->string ( 'phone' )->nullable ();
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
