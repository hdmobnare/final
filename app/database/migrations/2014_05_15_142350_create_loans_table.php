<?php
//Here we created our loans table using the migration functionality
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create ( 'loans', function ($table) {
			$table->increments ( 'id' );
			$table->datetime ( 'created_at' );
			$table->datetime ( 'updated_at' )->nullable ();
			$table->integer ( 'bookid' )->nullable ();
			$table->integer ( 'userid' )->nullable ();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('loans');
	}

}
