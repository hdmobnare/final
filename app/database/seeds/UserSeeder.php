<?php
class UserSeeder extends Seeder {
	public function run() {
		DB::table ( 'users' )->delete ();
		
		User::create ( array (
				'username' => 'admin',
				'password' => Hash::make ( 'password' ),
				'email' => 'admin@library.com',
				'role' => 'admin',
				'name' => 'George Harrisson',
				'address' => '13 Wellington Lane, Liverpool WA123',
				'phone' => '00442360909876' 
		) )

		;
		
		User::create ( array (
				'username' => 'librarian1',
				'password' => Hash::make ( 'password' ),
				'email' => 'librarian1@library.com',
				'role' => 'librarian',
				'name' => 'Paul McCartney',
				'address' => '261 Penny Lane, Liverpool WA444',
				'phone' => '00448758609754536'
		) )

		;
	}
}